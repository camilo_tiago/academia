﻿# Host: 127.0.0.1  (Version: 5.6.24)
# Date: 2016-11-28 20:22:21
# Generator: MySQL-Front 5.3  (Build 4.187)

/*!40101 SET NAMES utf8 */;

#
# Structure for table "equipamento"
#

DROP TABLE IF EXISTS `equipamento`;
CREATE TABLE `equipamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "equipamento"
#

INSERT INTO `equipamento` VALUES (1,'Supino Reto'),(2,'Crucifixo'),(3,'Supino Inclinado'),(4,'Pulley  '),(5,'Remada Baixa'),(6,'Agachamento '),(7,'Leg Press');

#
# Structure for table "horario"
#

DROP TABLE IF EXISTS `horario`;
CREATE TABLE `horario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `id_professor` int(11) DEFAULT NULL,
  `dia` int(11) DEFAULT NULL,
  `mes` int(11) DEFAULT NULL,
  `ano` int(11) DEFAULT NULL,
  `hora` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "horario"
#


#
# Structure for table "plano"
#

DROP TABLE IF EXISTS `plano`;
CREATE TABLE `plano` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "plano"
#

INSERT INTO `plano` VALUES (1,'Plano 1 (R$ 50 mensal)'),(2,'Plano 2 (R$ 200 semestre)');

#
# Structure for table "professor"
#

DROP TABLE IF EXISTS `professor`;
CREATE TABLE `professor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "professor"
#

INSERT INTO `professor` VALUES (1,'Professor 1'),(2,'Professor 2');

#
# Structure for table "treino"
#

DROP TABLE IF EXISTS `treino`;
CREATE TABLE `treino` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "treino"
#

INSERT INTO `treino` VALUES (1,'Treino A'),(2,'Treino B'),(3,'Treino A');

#
# Structure for table "treino_equipamento"
#

DROP TABLE IF EXISTS `treino_equipamento`;
CREATE TABLE `treino_equipamento` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_treino` int(11) DEFAULT NULL,
  `id_equipamento` int(11) DEFAULT NULL,
  `observacoes` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "treino_equipamento"
#

INSERT INTO `treino_equipamento` VALUES (1,1,1,'3x8'),(2,1,2,'3x8'),(3,1,3,'3x8'),(4,1,4,'4x8'),(5,2,5,'3x10'),(6,2,6,'3x10'),(7,2,7,'3x10'),(8,3,1,'3x12');

#
# Structure for table "usuario"
#

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senha` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dataNascimento` date DEFAULT NULL,
  `cpf` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telefone` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_plano` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "usuario"
#

INSERT INTO `usuario` VALUES (1,'Jose da Silva','usuario1','123456','1990-01-01','12345678900','usuario1@gmail.com','5199887766',2);

#
# Structure for table "usuario_treino"
#

DROP TABLE IF EXISTS `usuario_treino`;
CREATE TABLE `usuario_treino` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `id_treino` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "usuario_treino"
#

INSERT INTO `usuario_treino` VALUES (1,1,1),(2,1,2),(3,2,3);
