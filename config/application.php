<?php
error_reporting(E_ALL ^ ~E_NOTICE ^ ~E_WARNING);
date_default_timezone_set("America/Sao_Paulo");
session_start();

function __autoload($className) { 
      if (file_exists('app/model/entity/'.$className.'.php')) { 
          require_once 'app/model/entity/'.$className.'.php'; 
          return true; 
      } 	
      if (file_exists('app/model/'.$className.'.php')) { 
          require_once 'app/model/'.$className.'.php'; 
          return true; 
      } 
      if (file_exists('app/controller/'.$className.'.php')) { 
          require_once 'app/controller/'.$className.'.php'; 
          return true; 
      } 

      return false; 
} 
