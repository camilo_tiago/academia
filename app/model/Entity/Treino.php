<?php

class Treino {
	private $id;
	private $nome;
	private $Equipamentos;
	
	public function getId()
	{
	    return $this->id;
	}
	 
	public function setId($id)
	{
	    $this->id = $id;
	    return $this;
	}

	public function getNome()
	{
	    return $this->nome;
	}
	 
	public function setNome($nome)
	{
	    $this->nome = $nome;
	    return $this;
	}

	public function getEquipamentos()
	{
	    return $this->Equipamentos;
	}
	 
	public function setEquipamentos($Equipamentos)
	{
	    $this->Equipamentos = $Equipamentos;
	    return $this;
	}

}