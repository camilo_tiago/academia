<?php

class Professor {
	private $id;
	private $nome;
	private $Horarios;
	
	public function getId()
	{
	    return $this->id;
	}
	 
	public function setId($id)
	{
	    $this->id = $id;
	    return $this;
	}

	public function getNome()
	{
	    return $this->nome;
	}
	 
	public function setNome($nome)
	{
	    $this->nome = $nome;
	    return $this;
	}

	public function getHorarios()
	{
		$modelHorario = new HorarioModel();
		$this->Horarios = $modelHorario->findByParams(array("id_professor" => $this->id));
	    return $this->Horarios;
	}
	 
	public function setHorarios($Horarios)
	{
	    $this->Horarios = $Horarios;
	    return $this;
	}	

}