<?php

class Usuario {
	private $id;
	private $nome;
	private $login;	
	private $senha;
	private $dataNascimento;
	private $cpf;	
	private $email;
	private $telefone;	
	private $id_plano;		

	private $Horarios;
	
	public function getId()
	{
	    return $this->id;
	}
	 
	public function setId($id)
	{
	    $this->id = $id;
	    return $this;
	}

	public function getNome()
	{
	    return $this->nome;
	}
	 
	public function setNome($nome)
	{
	    $this->nome = $nome;
	    return $this;
	}

	public function getSenha()
	{
	    return $this->senha;
	}
	 
	public function setSenha($senha)
	{
	    $this->senha = $senha;
	    return $this;
	}

	public function getLogin()
	{
	    return $this->login;
	}
	 
	public function setLogin($login)
	{
	    $this->login = $login;
	    return $this;
	}

	public function getDataNascimento()
	{
	    return $this->dataNascimento;
	}
	 
	public function setDataNascimento($dataNascimento)
	{
	    $this->dataNascimento = $dataNascimento;
	    return $this;
	}

	public function getCpf()
	{
	    return $this->cpf;
	}
	 
	public function setCpf($cpf)
	{
	    $this->cpf = $cpf;
	    return $this;
	}

	public function getEmail()
	{
	    return $this->email;
	}
	 
	public function setEmail($email)
	{
	    $this->email = $email;
	    return $this;
	}

	public function getTelefone()
	{
	    return $this->telefone;
	}
	 
	public function setTelefone($telefone)
	{
	    $this->telefone = $telefone;
	    return $this;
	}

	public function getIdPlano()
	{
	    return $this->id_plano;
	}
	 
	public function setIdPlano($id_plano)
	{
	    $this->id_plano = $id_plano;
	    return $this;
	}

	public function getHorarios()
	{
		$modelHorario = new HorarioModel();
		$this->Horarios = $modelHorario->findByParams(array("id_usuario" => $this->id));
	    return $this->Horarios;
	}
	 
	public function setHorarios($Horarios)
	{
	    $this->Horarios = $Horarios;
	    return $this;
	}
}