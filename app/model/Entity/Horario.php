<?php

class Horario{
	private $id;
	private $Usuario;
	private $Professor;
	private $dia;
	private $mes;
	private $ano;
	private $hora; 

	public function getId()
	{
	    return $this->id;
	}
	 
	public function setId($id)
	{
	    $this->id = $id;
	    return $this;
	}

	public function getDia()
	{
	    return $this->dia;
	}
	 
	public function setDia($dia)
	{
	    $this->dia = $dia;
	    return $this;
	}

	public function getMes()
	{
	    return $this->mes;
	}
	 
	public function setMes($mes)
	{
	    $this->mes = $mes;
	    return $this;
	}

	public function getAno()
	{
	    return $this->ano;
	}
	 
	public function setAno($ano)
	{
	    $this->ano = $ano;
	    return $this;
	}

	public function getHora()
	{
	    return $this->hora;
	}
	 
	public function setHora($hora)
	{
	    $this->hora = $hora;
	    return $this;
	}

	public function getUsuario()
	{
	    return $this->Usuario;
	}
	 
	public function setUsuario($Usuario)
	{
	    $this->Usuario = $Usuario;
	    return $this;
	}

	public function getProfessor()
	{
	    return $this->Professor;
	}
	 
	public function setProfessor($Professor)
	{
	    $this->Professor = $Professor;
	    return $this;
	}
}