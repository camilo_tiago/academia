<?php

class EquipamentoModel extends Model {

	public function __construct(){
		$this->tableName = 'EQUIPAMENTO';
	}

	public function findByTreino($treino){
		$retorno = $this->execute("select *
									from equipamento e 
									join treino_equipamento te
									on te.id_equipamento = e.id
									where te.id_treino = {$treino->getId()}
									order by e.id");

		$equipamentosList = array();
		foreach($retorno as $register){
			$equipamento = new Equipamento();
			$equipamento->setId($register['id']);
			$equipamento->setNome($register['nome']);
			$equipamentosList[] = $equipamento;

		}
		return $equipamentosList;				
	}

}