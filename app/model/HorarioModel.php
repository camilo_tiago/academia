<?php

class HorarioModel extends Model {

	public function __construct(){
		$this->tableName = 'HORARIO';
	}

	//@override
	public function findByParams($paramsList, $orderBy = null){
		$retorno = parent::findByParams($paramsList, $orderBy);
		
		$modelUsuario = new UsuarioModel();
		$modelProfessor = new ProfessorModel();
		$horarioList = array();
		foreach($retorno as $register){
			$horario = new Horario();
			$horario->setId($register['id']);
			$horario->setUsuario($modelUsuario->findById($register['id_usuario']));
			$horario->setProfessor($modelProfessor->findById($register['id_professor']));
			$horario->setDia($register['dia']);
			$horario->setMes($register['mes']);
			$horario->setAno($register['ano']);
			$horario->setHora($register['hora']);
			$horarioList[] = $horario;
		}

		return $horarioList;		
	}


	public function findByUsuario(Usuario $usuario, $dia = null, $mes = null, $ano = null){
		$paramsList["id_usuario"] = $usuario->getId();
		if($dia) $paramsList["dia"] = $dia;
		if($mes) $paramsList["mes"] = $mes;
		if($ano) $paramsList["ano"] = $ano;
		$orderBy = "dia, mes, ano, hora";
		return $this->findByParams($paramsList, $orderBy);
	}


	public function findByProfessor(Professor $professor, $dia = null, $mes = null, $ano = null){
		$paramsList["id_professor"] = $professor->getId();
		if($dia) $paramsList["dia"] = $dia;
		if($mes) $paramsList["mes"] = $mes;
		if($ano) $paramsList["ano"] = $ano;
		
		return $this->findByParams($paramsList);
	}


	public function save(Horario $horario){
		$this->execute("INSERT INTO HORARIO 
			(ID_USUARIO
			,ID_PROFESSOR
			,DIA
			,MES
			,ANO
			,HORA) 
			VALUES 
			('{$horario->getUsuario()->getId()}'
			,'{$horario->getProfessor()->getId()}'
			,'{$horario->getDia()}'
			,'{$horario->getMes()}'
			,'{$horario->getAno()}'
			,'{$horario->getHora()}'
			)");
	}

	public function deleteFromDay(Horario $horario){
		$this->execute("DELETE FROM HORARIO WHERE ID_USUARIO = ".$horario->getUsuario()->getId()." AND DIA = ".$horario->getDia());
	}			
}