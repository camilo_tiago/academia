<?php

class TreinoModel extends Model {

	public function __construct(){
		$this->tableName = 'TREINO';
	}

	public function findByUsuario($usuario){
		$retorno = $this->execute("select *
									from treino t
									join usuario_treino ut
									on ut.id_treino = t.id
									where ut.id_usuario = {$usuario->getId()}
									order by t.id");

		$modelEquipamento = new EquipamentoModel();
		$treinoList = array();
		foreach($retorno as $register){
			$treino = new Treino();
			$treino->setId($register['id']);
			$treino->setNome($register['nome']);
			$treino->setEquipamentos($modelEquipamento->findByTreino($treino));			
			$treinoList[] = $treino;
		}
		
		return $treinoList;				
	}

}