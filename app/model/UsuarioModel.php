<?php

class UsuarioModel extends Model {

	public function __construct(){
		$this->tableName = 'USUARIO';
	}

	public function save(Usuario $usuario){
		$this->execute("INSERT INTO USUARIO 
			(NOME
			,LOGIN
			,SENHA
			,DATANASCIMENTO
			,CPF
			,TELEFONE
			,EMAIL					
			,ID_PLANO			
			) 
			VALUES 
			('{$usuario->getNome()}'
			,'{$usuario->getLogin()}'
			,'{$usuario->getSenha()}'
			,'{$usuario->getDataNascimento()}'
			,'{$usuario->getCpf()}'
			,'{$usuario->getTelefone()}'
			,'{$usuario->getEmail()}'												
			,'{$usuario->getIdPlano()}'				
			)");
	}

	public function authenticate($login, $senha){
		$paramsList["login"] = $login;
		$paramsList["senha"] = $senha;
		$retorno = $this->findByParams($paramsList);
		
		$usuario = new Usuario();
		
		foreach($retorno as $register){
			$usuario->setId($register['id']);
			$usuario->setNome($register['nome']);
			$usuario->setSenha($register['login']);
			$usuario->setLogin($register['senha']);
		}
		

		return $usuario;
	}

	//@override
	public function findById($key){
		$retorno = parent::findById($key);

		$usuario = new Usuario();
		
		foreach($retorno as $register){
			$usuario->setId($register['id']);
			$usuario->setNome($register['nome']);
			$usuario->setSenha($register['login']);
			$usuario->setLogin($register['senha']);
		}
		
		return $usuario;		
	}


}