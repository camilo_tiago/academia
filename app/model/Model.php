<?php
include_once('config/database.php');

abstract class Model {
	protected $tableName;

	protected function execute($query){
		//Obtem conexao com banco de dados
		global $databaseConnection;

		//Executa SQL (SELECT | INSERT | UPDATE | DELETE)
		$result = mysql_query($query, $databaseConnection);  

		// Se for um SELECT retornara um resource
		if(is_resource($result)){
			$registerList = array();
			while($register = mysql_fetch_array($result)) {
			   $registerList[] = $register; 
			}
			return $registerList;
		}

		// Se for um (INSERT | UPDATE | DELETE) retorna a quantidade de linhas afetadas
		return mysql_affected_rows();		
	}


	public function findAll(){
		$retorno = $this->execute("SELECT * FROM {$this->tableName}");
		
		return $retorno;
	}

	public function findById($key){
		$query = "SELECT * FROM {$this->tableName} WHERE id = {$key}";
		$retorno = $this->execute($query);
		
		return $retorno;
	}	

	public function findByParams($paramsList, $orderBy = null){
		foreach($paramsList as $key => $value){
			$paramsStringList[] = "{$key} = '{$value}'";
		}

		$query = "SELECT * FROM {$this->tableName} WHERE ".implode($paramsStringList," AND ");
		if($orderBy) $query .= " ORDER BY ".$orderBy;		
		$retorno = $this->execute($query);
		
		return $retorno;
	}	

}