<?php

class IndexController extends Controller {


	public function index(){

	}

	public function login(){
		if(isset($_POST['login']) && isset($_POST['senha'])){
			$modelUsuario = new UsuarioModel();
			$usuarioLogado = $modelUsuario->authenticate($_POST['login'], $_POST['senha']);

			if($usuarioLogado->getId()){
				$_SESSION['usuario'] = $usuarioLogado;
				
				$this->setView('Index/index');
			}
		}
	}	

	public function logout(){
		session_destroy();
		$this->setView('Index/login');
	}

}