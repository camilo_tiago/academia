<?php

class HorarioController extends Controller {

	public function index(){
		$usuarioLogado = $_SESSION['usuario'];

		if(count($_POST)){
			$data = explode("/",$_POST["data"]);
			$dia = $data[0];
			$mes = $data[1];
			$ano = $data[2];

			$professor = new Professor();
			$professor->setId($_POST['professor']);

			$modelHorario = new HorarioModel();
			//$horariosUsuario = $modelHorario->findByUsuario($usuarioLogado, $dia, $mes, $ano);
			$this->viewData->horariosProfessor = $modelHorario->findByProfessor($professor, $dia, $mes, $ano);
			$this->viewData->professor = $professor->getId();
			$this->viewData->usuarioLogado = $usuarioLogado;
			$this->viewData->dia = $dia;
			$this->viewData->mes = $mes;
			$this->viewData->ano = $ano;

			$this->setView('Horario/save');
		}else{
			$modelProfessor = new ProfessorModel();
			$this->viewData->professores = $modelProfessor->findAll();
			$modelHorario = new HorarioModel();
			$this->viewData->horarios = $modelHorario->findByUsuario($usuarioLogado);
		}

	}

	public function save(){


		if(count($_POST)){
			$usuarioLogado = $_SESSION['usuario'];
			$professor = new Professor();
			$professor->setId($_POST["professor"]);

			$horario = new Horario();
			$horario->setUsuario($usuarioLogado);
			$horario->setProfessor($professor);
			$horario->setDia($_POST["dia"]);
			$horario->setMes($_POST["mes"]);
			$horario->setAno($_POST["ano"]);
			$horario->setHora($_POST["hora"]);

			$modelHorario = new HorarioModel();
			$modelHorario->deleteFromDay($horario);
			$modelHorario->save($horario);

			$this->setView("Index/index");
		}

	}

}
