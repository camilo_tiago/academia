<?php

class UsuarioController extends Controller {

	public function save(){

		if(count($_POST)){			
			$usuario = new Usuario();

			$usuario->setNome($_POST["nome"]);
			$usuario->setLogin($_POST["login"]);
			$usuario->setSenha($_POST["senha"]);
			$usuario->setDataNascimento($_POST["dtNascimento"]);
			$usuario->setCpf($_POST["cpf"]);
			$usuario->setEmail($_POST["email"]);
			$usuario->setTelefone($_POST["telefone"]);
			$usuario->setIdPlano($_POST["idPlano"]);

			$modelUsuario = new UsuarioModel();
			$modelUsuario->save($usuario);

			$this->viewData->message = "Cadastrado com sucesso!";
			$this->setView('index/login');
		}else{
			$modelPlano = new PlanoModel();
			$this->viewData->planos = $modelPlano->findAll();
		}

	}

}