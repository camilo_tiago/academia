<?php

class TreinoController extends Controller {

	public function index(){
		$usuarioLogado = $_SESSION['usuario'];		
		$modelTreino = new TreinoModel();
		$this->viewData->treinos = $modelTreino->findByUsuario($usuarioLogado);
	}

}