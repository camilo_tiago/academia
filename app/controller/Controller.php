
<?php

abstract class Controller {
	protected $viewData;
	protected $view;

	public function getViewData()
	{
	    return $this->viewData;
	}
	 
	public function setViewData($viewData)
	{
	    $this->viewData = $viewData;
	    return $this;
	}

	public function getView()
	{
	    return $this->view;
	}
	 
	public function setView($view)
	{
	    $this->view = $view;
	    return $this;
	}

}
