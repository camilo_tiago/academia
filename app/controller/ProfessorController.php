<?php

class ProfessorController extends Controller {

	public function index(){
		$modelProfessor = new ProfessorModel();
		$this->viewData->professores = $modelProfessor->findAll();		
	}

	public function view(){
		$professor = new Professor();
		$professor->setId($_REQUEST["id"]);
		
		$modelHorario = new HorarioModel();		
		$horarios = $modelHorario->findByProfessor($professor);
		
		$listaHorarioOcupado = array();
		foreach($horarios as $horario){
			$listaHorarioOcupado[$horario->getDia()][$horario->getMes()][$horario->getAno()][$horario->getHora()] = true;
		}

		$begin = new DateTime();
		$end = new DateTime();
		$end = $end->add(new DateInterval('P7D')); 

		$interval = new DateInterval('P1D');
		$daterange = new DatePeriod($begin, $interval ,$end);

		foreach($daterange as $date){
			for($hora = 8; $hora <= 16; $hora ++){
				if($listaHorarioOcupado[$date->format("d")][$date->format("m")][$date->format("Y")][$hora])
						continue;
		    	$this->viewData->horariosLivres[$date->format("d/m/Y")][] = $hora;
			}
		}				
	}	

}