<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title> Criar nova conta </title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="public/css/application.css" />

  <!-- JS -->
</head>
<body>
  <div id="id01">
    <div id="header">
      <img src="public/img/logo.png" class="center-block">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <form class="modal-content" action="index.php?controller=Usuario&action=save" method="POST" style="padding: 30px 15px;overflow: hidden;box-shadow: none;border: 1px solid #bbb;">
            <label><b>Nome</b></label>
            <input type="text" placeholder="Digite seu Nome" name="nome" required>

            <br/> <br/> 

            <label><b>Login</b></label>
            <input type="text" placeholder="Digite seu Login" name="login" required>

            <br/> <br/> 

            <label><b>Senha</b></label>
            <input type="password" placeholder="Digite sua Senha" name="senha" required>

            <br/> <br/> 

            <label><b>Data Nascimento</b></label>
            <input type="date" placeholder="Digite sua Data Nascimento" name="dtNascimento" required>

            <br/> <br/> 

            <label><b>CPF</b></label>
            <input type="number" placeholder="Digite seu CPF (somente números)" name="cpf" required max="99999999999">
            
            <br/> <br/> 

            <label><b>E-mail</b></label>
            <input type="email" placeholder="Digite seu E-mail" name="email" required>
            
            <br/> <br/> 

            <label><b>Telefone</b></label>
            <input type="tel" placeholder="Digite seu Telefone" name="telefone" required>
                                                            

            <label><b>Plano</b></label>
            <select name="idPlano">
              <?php foreach($viewData->planos as $plano):?>
                <option value="<?php echo $plano['id'] ?>"><?php echo $plano['nome'] ?></option>
              <?php endforeach; ?>              
            </select>

            <button type="btn submit">Salvar</button>
          </form>
        </div>
      </div>      
    </div>
  </div>

</body>
</html>
