<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title> Nome do APP - Página de Login </title>
  <!-- CSS -->
  <link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="public/css/application.css" />

  <!-- JS -->
</head>
<body>
  <div id="id01">
    <div id="header">
      <img src="public/img/logo.png" class="center-block">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-md-offset-3">
          <form class="modal-content" action="index.php?controller=Index&action=login" method="POST" style="padding: 30px 15px 0px;overflow: hidden;box-shadow: none;border: 1px solid #bbb;">
            <label><b>Login</b></label>
            <input type="text" placeholder="Digite seu Login" name="login" required>

            <br/> <br/> 

            <label><b>Senha</b></label>
            <input type="password" placeholder="Digite sua Senha" name="senha" required>
              
            <button type="btn submit">Login</button>
            <?php if ($viewData->message) echo $viewData->message ?>

            <hr style="margin: 20px -15px 0px; border-top:1px solid #ccc; margin-">
            <div style="background-color:#f1f1f1; margin: 0 -15px; padding: 15px;" class="text-center">      
              <a href="index.php?controller=Usuario&action=save"> Não possui conta? Cadastre-se! </a>
            </div>
          </form>
        </div>
      </div>      
    </div>
  </div>

</body>
</html>

