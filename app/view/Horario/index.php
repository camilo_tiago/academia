<?php 
require("menu.php");
?>	

		<div class="container inner">
			<div class="row">
				<div class="col-md-12" id="breadcrumb">
					<span></span> <a href="index.php?controller=Horario&action=index"> Horários </a> / Consultar
				</div>
			</div>
			<form action="index.php?controller=Horario&action=index" method="POST">
				<div class="row">
					<div class="col-md-6">
						<label><b>Professor</b></label>
						<select  name="professor" required class="form-control">  
							<option value=""> Selecione uma opção... </option>  
							<?php foreach($viewData->professores as $professor):?>
								<option value="<?php echo $professor->getId() ?>"><?php echo $professor->getNome() ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="col-md-4">
						<label><b>Data</b></label>
						<input type="date"  name="data" class="form-control" required>
					</div>
					<div class="col-md-2">
						<button type="submit" style="width: 100%; margin-top: 24px;">Consultar</button>		
					</div>
				</div>

				<hr>

				<div class="row">
					<div class="col-md-12">
						<h3 style="text-transform:uppercase; margin: 40px 0 15px; font-size: 18px; text-align: center;"><strong> Seus Horários </strong></h3>
						<table class="table table-striped table-hover">
							<thead>
								<th> Professor </th>
								<th> Data </th>
								<th> Horário </th>
							</thead>
							<tbody>
							<?php foreach($viewData->horarios as $horario):?>
								<?php echo "
									<tr>
										<td> {$horario->getProfessor()->getNome()} </td>
										<td> {$horario->getDia()}/{$horario->getMes()}/{$horario->getAno()}</td>
										<td> {$horario->getHora()}h:00min</td>
									</tr>
								";
								endforeach; ?>								
							</tbody>
					</div>
				</div>				
			</form>
		</div>
	</div>
</body>
</html>
