<?php 
require("menu.php");
?>	

	<div class="container inner">
		<div class="row">
			<div class="col-md-12" id="breadcrumb">
				<span></span> <a href="index.php?controller=Horario&action=index"> Horários </a> / Salvar
			</div>
		</div>
		<form action="index.php?controller=Horario&action=save" method="POST">
	    	<input type="hidden" name="professor" value="<?php echo $viewData->professor?>" />
	    	<input type="hidden" name="dia" value="<?php echo $viewData->dia?>" />
	    	<input type="hidden" name="mes" value="<?php echo $viewData->mes?>" />
	    	<input type="hidden" name="ano" value="<?php echo $viewData->ano?>" />
			
			<div class="row">
	    		<div class="col-md-6">
				<?php 
					for($hora = 8; $hora <=16 ; $hora ++): ?>
						<label class="text-right" style="width: 50px;margin-right: 10px;"><b><?php echo $hora; ?> hs:</b></label>
						<input type="radio" name="hora" value="<?php echo $hora; ?>" required								
				<?php 
						if(is_array($viewData->horariosProfessor)):							
							foreach ($viewData->horariosProfessor as $horario):								
								if($horario->getHora() == $hora 
									&& $viewData->professor == $horario->getProfessor()->getId()) :
									if($viewData->usuarioLogado->getId() == $horario->getUsuario()->getId()): ?>
										checked="checked"
									<?php else: ?>
										disabled="disabled"
						<?php 		endif;
								endif;
							endforeach;
						endif; 
				?>
				>
				<br />
	      		<?php endfor;?>
	      		</div>
	      	</div>
	      	<hr>
	      	<div class="row">
	      		<div class="col-md-12">
					<button type="submit" style="width: 150px;">Salvar</button>		
				</div>
			</div>
		</div>	  	
  	</form>  	
</div>

</body>
</html>
