<?php 
require("menu.php");
?>	

		<div class="container inner">
			<div class="row">
				<div class="col-md-12" id="breadcrumb">
					<span></span> <a href="index.php?controller=Professor&action=index"> Professores </a> / Consultar
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<table class="table table-striped table-hover">
						<thead>
							<th width="80%"> Professor </th>
							<th width="20%"> Ação </th>
						</thead>
						<tbody>
						<?php foreach($viewData->professores as $professor):
								echo "
									<tr>
										<td style='vertical-align:middle'> {$professor->getNome()} </td>
										<td> 
											<a style='color: #fff' class='btn btn-danger' href='index.php?controller=Professor&action=view&id={$professor->getId()}'> 
												Visualizar
											</a>
										</td>
									</tr>
								";
							endforeach; ?>								
						</tbody>
					</table>
				</div>
			</div>				
		</div>
	</div>
</body>
</html>
