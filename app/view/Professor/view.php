<?php 
require("menu.php");
?>	
	<div class="container inner">
		<div class="row">
			<div class="col-md-12" id="breadcrumb">
				<span></span> <a href="index.php?controller=Professor&action=index"> Professores </a> / Horários Livres
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3 style="text-transform:uppercase; margin: 20px 0 15px; font-size: 18px; text-align: center;"><strong> Horários Livres </strong></h3>
				<?php 
					foreach($viewData->horariosLivres as $dia => $horas):?>
						<table class="table table-striped table-hover">
							<thead>
								<th width="100%" style="text-align: center;"> <?php echo $dia ?> </th>
							</thead>
							<tbody>
							<?php 
								$hour = 8;
								$free_hour = array();
								
								foreach($horas as $hora):
									$free_hour[] = $hora;
								endforeach; 

								for($i = 8; $i <= 16; $i++) {
									if(in_array($i, $free_hour)){
										$bg = ($i % 2 == 0)? '#d3eaca' : '#dff0d8';
										echo "
											<tr style='color: #3c763d;background-color: {$bg};border-color: #d6e9c6;'>
											<td>{$hour}h:00min  <span class='pull-right btn btn-success btn-xs'>Disponível</span></td>
											</tr>
										";
									}
									else{
										$bg = ($i % 2 == 0)? '#ecd0d0' : '#f2dede';
										echo "
											<tr style='color: #a94442; background-color: {$bg}; border-color: #ebccd1;'>
											<td>{$hour}h:00min <span class='pull-right btn btn-danger btn-xs'>Reservado</span></td>
											</tr>
										";
									}

									$hour++;
								}
							?>
							</tbody>
						</table>
					<?php endforeach; ?>
				</div>
			</div>				
		</div>
	</div>

	<br/><br/>
</body>
</html>