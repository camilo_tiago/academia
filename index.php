<?php
include_once('config/application.php');

if(isset($_REQUEST['controller']) && isset($_REQUEST['action'])){
	$controllerName = $_REQUEST['controller'];
	$actionName = $_REQUEST['action'];
}else{
	$controllerName = 'Index';
	$actionName = "login";	
}

$controllerNameClass = $controllerName.'Controller';

$controller = new $controllerNameClass();
$controller->$actionName();


$usuarioLogado = $_SESSION['usuario'];
$viewData = $controller->getViewData();

if($controller->getView()){
	include("app/view/{$controller->getView()}.php");
	//header("Location: app/view/{$controller->getView()}.php");
}else{
	include("app/view/{$controllerName}/{$actionName}.php");
}