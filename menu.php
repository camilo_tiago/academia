<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title> Nome do APP - Home</title>

		<!-- CSS -->
		<link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="public/css/application.css" />

		<!-- JS -->
		<script type="text/javascript" src="public/js/jquery.min.js"></script>
		<script type="text/javascript" src="public/js/bootstrap.min.js"></script>
	</head>
<body>
	<div id="id01">
	  	<nav class="navbar navbar-default" id="header">
	  		<div class="container" style="padding: 0 15px;">
	  			<div class="row">
					<div class="container-fluid">
						<div class="navbar-header">
							<a class="navbar-brand" href="index.php"> 
							<img src="public/img/logo.png">
							</a>
						</div>
						<ul class="nav navbar-nav navbar-right">
							<li <?php echo $_GET['controller'] == 'Index' ? 'class="active"' : '' ?>><a href="index.php?controller=Index&action=index">Home</a></li>
							<li <?php echo $_GET['controller'] == 'Horario' ? 'class="active"' : '' ?>><a href="index.php?controller=Horario&action=index">Alterar Horário</a></li>
							<li <?php echo $_GET['controller'] == 'Professor' ? 'class="active"' : '' ?>><a href="index.php?controller=Professor&action=index">Visualizar Professor</a></li>
							<li <?php echo $_GET['controller'] == 'Treino' ? 'class="active"' : '' ?>><a href="index.php?controller=Treino&action=index">Visualizar Treino</a></li>
		
							<li class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#"><?= $usuarioLogado->getNome(); ?>
								<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="index.php?controller=Index&action=logout">Logout</a></li>
								</ul>
							</li> 
						</ul>
					</div>
				</div>
			</div>
		</nav>